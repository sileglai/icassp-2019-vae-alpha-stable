#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2019 by Inria
Authored by Simon Leglaive (simon.leglaive@inria.fr)
License agreement in LICENSE.txt
"""

import os
my_seed = 0
import numpy as np
np.random.seed(my_seed)
from VAE import VAE
from data_tools import (compute_STFT_data_from_file_list_TIMIT,
                        load_STFT_data_in_array_TIMIT,
                        write_VAE_params_to_text_file,
                        write_VAE_params_to_pckl_file)
import pickle
from keras import backend as KB
import datetime
import librosa

#%% Define paths

# Directory containing the training set of the TIMIT dataset
TIMIT_train_folder = '/local_scratch/sileglai/datasets/clean_speech/TIMIT/TRAIN'

# Pickle file containing the training data
data_dir = 'data'
data_file = os.path.join(data_dir, 'training_data.pckl')

# Directory for writting the results of the training
results_dir = 'training_results'

# Create the directories if necessary
if not(os.path.isdir(data_dir)):
    os.makedirs(data_dir)

if not(os.path.isdir(results_dir)):
    os.makedirs(results_dir)

#%% Compute training data

fs = int(16e3) # Sampling rate
wlen_sec = 64e-3 # STFT window length in seconds
hop_percent = 0.25  # hop size as a percentage of the window length
trim = True # trim leading and trailing silences

# List containing all the wav files in the training set of TIMIT
# The number of elements should be 4620 (462 speakers x 10 sentences)
file_list = librosa.util.find_files(TIMIT_train_folder, ext='wav')

compute_STFT_data_from_file_list_TIMIT(file_list,
                                        fs=fs,
                                        wlen_sec=wlen_sec,
                                        hop_percent=hop_percent,
                                        zp_percent=0,
                                        trim=trim,
                                        verbose=True,
                                        out_file=data_file)

#%% Parameters

description = 'Using the entire training database of TIMIT'
representation = 'power_spec'
latent_dim = 64 # Dimension of the latent random vector
intermediate_dim = [128] # Size of the intermediate non-linear layer
batch_size = 128
activation = 'tanh'
epochs = 500
shuffle = True
early_stopping_patience = 10
monitor_early_stopping = 'val_loss'
valid_set_percent = 0.2
date = datetime.datetime.now().strftime("%Y-%m-%d-%Hh%M")

verbose_data = True
verbose_vae = 2 # Verbosity mode. 0 = silent, 1 = progress bar, 2 = one line per epoch.
display_network = True

plot_reconstruction = True

#%% Load the training and validation data

num_files_tot = None # If None, all the files are considered

[power_spec, phase, data_info, fs, wlen_sec, hop_percent, trim, num_files_tot] = load_STFT_data_in_array_TIMIT(data_file,
                                                                                            ind_first_file=0,
                                                                                            num_files=num_files_tot,
                                                                                            verbose=verbose_data)
N_tot = power_spec.shape[-1]

num_files_train = int((1-valid_set_percent)*num_files_tot) # We take 80 % of the files for training

ind_beg_val = data_info[num_files_train]['index_begin'] # Get the index of the first frame of the first file in the validation set

N_train = ind_beg_val//batch_size*batch_size # Make the number of training examples a multiple of the batch_size

N_val = (N_tot - ind_beg_val)//batch_size*batch_size # Make the number of validation examples a multiple of the batch_size

x_train = power_spec[:,0:N_train].T
phase_train = phase[:,0:N_train]

x_val = power_spec[:,ind_beg_val:ind_beg_val+N_val].T
phase_val = phase[:,N_train:]

del power_spec, phase

#%%  Build the VAE

input_dim = x_train.shape[-1]

vae = VAE(input_dim=input_dim, latent_dim=latent_dim, intermediate_dim=intermediate_dim, batch_size=batch_size, activation=activation)

vae.build()

if display_network:
    vae.print_model()

#%% Train the VAE

# Define loss
def vae_loss(data_orig, data_reconstructed):
    # Reconstruction term, equivalent to the IS divergence between x_orig and x_reconstructed
    reconstruction_loss = KB.sum( data_reconstructed + data_orig/KB.exp(data_reconstructed), axis=-1 )
    # Regularization term
    kl_loss = - 0.5 * KB.sum(vae.model.get_layer('latent_log_var').output
                                - KB.square(vae.model.get_layer('latent_mean').output)
                                - KB.exp(vae.model.get_layer('latent_log_var').output ), axis=-1)

    return reconstruction_loss + kl_loss

# Compile model
vae.compile(optimizer='adam', loss=vae_loss)

# Train VAE
early_stopping_dic = {'monitor':monitor_early_stopping, 'min_delta':0, 'patience':early_stopping_patience, 'verbose':1, 'mode':'min'}
history = vae.train(data_train=x_train,
                    data_val=x_val,
                    verbose=verbose_vae,
                    shuffle=shuffle,
                    epochs=epochs,
                    early_stopping_dic=early_stopping_dic)

#%% Save parameters

dic_params = {'representation':representation,
          'input_dim':input_dim,
          'latent_dim':latent_dim,
          'intermediate_dim':intermediate_dim,
          'batch_size':batch_size,
          'activation':activation,
          'epochs':epochs,
          'shuffle':shuffle,
          'early_stopping_patience':early_stopping_patience,
          'monitor_early_stopping':monitor_early_stopping,
          'my_seed':my_seed,
          'description':description,
          'valid_set_percent':valid_set_percent,
          'date':date}

# Write the VAE and training parameters to a text file
parms_text_file = os.path.join(results_dir, 'parameters.txt')
write_VAE_params_to_text_file(parms_text_file, dic_params)

# Write the VAE and training parameters to a pickle file
parms_pckl_file = os.path.join(results_dir, 'parameters.pckl')
write_VAE_params_to_pckl_file(parms_pckl_file, dic_params)

#%% Save weights to an h5 file

weights_file = os.path.join(results_dir, 'saved_weights.h5')
vae.save_weights(weights_file)

#%% Save the training loss

loss_file = os.path.join(results_dir, 'training_loss.pckl')
with open(loss_file, 'wb') as f:
        pickle.dump(vae.history.history, f)

#%% Check the reconstruction on a few frames of the validation set

if plot_reconstruction:

    wlen = wlen_sec*fs # window length of 64 ms
    wlen = np.int(np.power(2, np.ceil(np.log2(wlen)))) # next power of 2
    hop = np.int(wlen*hop_percent) # hop size

    import matplotlib.pyplot as plt
    data_decoded = vae.encode_decode(x_val[0:1024,:])
    data_decoded = np.exp(data_decoded) # The output is the log of the variance, so we have to take the exponential

    plt.figure()
    plt.subplot(2, 1, 1)
    librosa.display.specshow(librosa.power_to_db(x_val[0:1024,:].T), y_axis='log', sr=fs, hop_length=hop, vmin=-50, vmax=20)
    plt.set_cmap('jet')
    plt.colorbar(format='%+2.0f dB')
    plt.title('Original spectrogram')

    plt.subplot(2, 1, 2)
    librosa.display.specshow(librosa.power_to_db(data_decoded.T), x_axis='time', y_axis='log', sr=fs, hop_length=hop, vmin=-50, vmax=20)
    plt.set_cmap('jet')
    plt.colorbar(format='%+2.0f dB')
    plt.title('Reconstructed spectrogram')
