representation:power_spec
input_dim:513
latent_dim:16
intermediate_dim:[128]
batch_size:128
activation:tanh
epochs:500
shuffle:True
early_stopping_patience:10
monitor_early_stopping:val_loss
my_seed:0
description:Using the entire training database of TIMIT
valid_set_percent:0.2
date:2018-03-27-11h56
