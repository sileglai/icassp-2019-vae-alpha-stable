#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2019 by Inria
Authored by Simon Leglaive (simon.leglaive@inria.fr)
License agreement in LICENSE.txt
"""


import numpy as np
from scipy.stats import levy_stable as alpha_stable


class MCEM_algo:
    def __init__(self, X=None, H=None, Phi=None, decoder=None, alpha=1,
                 niter_MCEM=100, niter_MCMC=40, burnin=30, epsilon2=0.01):
        self.X = X # Mixture STFT, shape (F,N)
        self.H = H # Last draw of the VAE latent variables, shape (D, N)
        self.Phi = Phi # Last draw of the impulse latent variables, shape (D, N)
        self.alpha = alpha # Shape parameter of the alpha stable noise model
        self.scale_PaS = 2*np.cos(np.pi*self.alpha/4)**(2/self.alpha)
        self.decoder = decoder # VAE decoder, keras model
        self.niter_MCEM = niter_MCEM # Maximum number of MCEM iterations
        self.niter_MCMC = niter_MCMC # Number of iterations for the MCMC algorithm
        # at the E-step
        self.burnin = burnin # Burn-in period for the MCMC algorithm at the
        # E-step
        self.epsilon2 = epsilon2 # Variance of the proposal distribution for
        # the VAE latent variables
        self.g = np.ones((1,self.X.shape[1])) # gain parameters, shape (1,N)
        self.sigma_b2 = np.ones((self.X.shape[0],1)) # noise variance, shape (F,1)
        self.H_mapped_decoder = np.exp(self.decoder.decode(self.H.T).T) # output
        # of the decoder with self.H as input, shape (F, N)
        self.speech_var = self.H_mapped_decoder*self.g # apply gain

    def metropolis_hastings_within_gibbs(self, niter=None, burnin=None):

        if niter==None:
           niter = self.niter_MCMC

        if burnin==None:
           burnin = self.burnin

        F, N = self.X.shape
        D = self.H.shape[0]### Discard samples drawn during the burn-in period


        H_sampled = np.zeros((D, N, niter - burnin))
        Phi_sampled = np.zeros((F, N, niter - burnin))

        cpt = 0
        averaged_acc_rate_H = 0
        averaged_acc_rate_Phi = 0
        for n in np.arange(niter):

            ### Full conditional for the VAE latent variables

            # Sample from proposal

            H_prime = self.H + np.sqrt(self.epsilon2)*np.random.randn(D,N)
            H_prime_mapped_decoder = np.exp(self.decoder.decode(H_prime.T).T)
            # shape (F, N)
            speech_var_prime = H_prime_mapped_decoder*self.g # apply gain

            # Compute acceptance ratio10

            acc_prob_H = ( np.sum( np.log(self.speech_var + self.Phi*self.sigma_b2)
                         - np.log(speech_var_prime + self.Phi*self.sigma_b2)
                         + ( 1/(self.speech_var + self.Phi*self.sigma_b2)
                         - 1/(speech_var_prime + self.Phi*self.sigma_b2) )
                         * np.abs(self.X)**2, axis=0)
                         + .5*np.sum( self.H**2 - H_prime**2 , axis=0) )

            # Accept-reject

            is_acc_H = np.log(np.random.rand(1,N)) < acc_prob_H
            is_acc_H = is_acc_H.reshape((is_acc_H.shape[1],))

            averaged_acc_rate_H += np.sum(is_acc_H)/np.prod(is_acc_H.shape) \
                                 *100/niter

            self.H[:,is_acc_H] = H_prime[:,is_acc_H]
            self.H_mapped_decoder = np.exp(self.decoder.decode(self.H.T).T)
            self.speech_var = self.H_mapped_decoder*self.g

            ### Full conditional for the impulse latent variables

            # Sample from proposal

            Phi_prime = alpha_stable.rvs(self.alpha/2, 1, loc=0,
                                         scale=self.scale_PaS, size=(F,N))

            # Compute acceptance ratio

            acc_prob_Phi = ( np.log(self.speech_var + self.Phi*self.sigma_b2)
                            - np.log(self.speech_var + Phi_prime*self.sigma_b2)
                            + ( 1/(self.speech_var + self.Phi*self.sigma_b2)
                            - 1/(self.speech_var + Phi_prime*self.sigma_b2) )
                            * np.abs(self.X)**2 )

            # Accept-reject

            is_acc_Phi = np.log(np.random.rand(F,N)) < acc_prob_Phi

            averaged_acc_rate_Phi += np.sum(is_acc_Phi)/np.prod(is_acc_Phi.shape) \
                                 *100/niter

            self.Phi[is_acc_Phi] = Phi_prime[is_acc_Phi]

            ### Discard samples drawn during the burn-in period

            if n > burnin - 1:
                H_sampled[:,:,cpt] = self.H
                Phi_sampled[:,:,cpt] = self.Phi
                cpt += 1

        return H_sampled, Phi_sampled


    def run(self):

        F, N = self.X.shape
        D = self.H.shape[0]

        X_abs_2 = np.abs(self.X)**2

        cost_after_M_step = np.zeros((self.niter_MCEM, 1))

        for n in np.arange(self.niter_MCEM):

            print("iter %d/%d" % (n+1, self.niter_MCEM))

            ### MC-E-Step

            print('MC-E-Step')

            H_sampled, Phi_sampled = self.metropolis_hastings_within_gibbs(self.niter_MCMC,
                                                              self.burnin)

            H_sampled_mapped_decoder = np.exp(
                    self.decoder.decode(H_sampled.reshape(
                            (D, N*(self.niter_MCMC-self.burnin) ) ).T).T)
            H_sampled_mapped_decoder = H_sampled_mapped_decoder.reshape(
                    (F, N, self.niter_MCMC - self.burnin) )  # shape (F,N,R)
            speech_var_multi_samples = (H_sampled_mapped_decoder*
                                        self.g[:,:,None]) # shape (F,N,R)

            ### M-Step

            V_x = Phi_sampled*self.sigma_b2[:,:,np.newaxis] + speech_var_multi_samples # (F, N, R)

            print('M-Step - noise variance')
            self.sigma_b2[:,0] = self.sigma_b2[:,0]*(
                    (np.sum(X_abs_2 * np.sum(Phi_sampled*(V_x**-2),
                                                    axis=-1), axis=-1))
                    /(np.sum(np.sum(Phi_sampled*(V_x**-1),
                                           axis=-1), axis=-1))
                    )**.5

            V_x = Phi_sampled*self.sigma_b2[:,:,np.newaxis] + speech_var_multi_samples

            print('M-Step - gain parameters')
            self.g = self.g*(
                    (np.sum(X_abs_2 * np.sum(
                            H_sampled_mapped_decoder*(V_x**-2),
                            axis=-1), axis=0) )
                    /(np.sum(np.sum(
                            H_sampled_mapped_decoder*(V_x**-1),
                            axis=-1), axis=0) ) )**.5

            speech_var_multi_samples = (H_sampled_mapped_decoder*
                                        self.g[:,:,None]) # shape (F,N,R)

            V_x = Phi_sampled*self.sigma_b2[:,:,np.newaxis] + speech_var_multi_samples

            cost_after_M_step[n] = np.mean(
                    np.log(V_x)
                    + X_abs_2[:,:,None]/V_x )

            print("cost=%.4f\n" % cost_after_M_step[n])

        return cost_after_M_step

    def separate(self, niter_MCMC=None, burnin=None):

        if niter_MCMC==None:
           niter_MCMC = self.niter_MCMC

        if burnin==None:
           burnin = self.burnin

        F, N = self.X.shape
        D = self.H.shape[0]

        H_sampled, Phi_sampled = self.metropolis_hastings_within_gibbs(niter_MCMC, burnin)

        H_sampled_mapped_decoder = np.exp(
                self.decoder.decode(
                        H_sampled.reshape(
                                ( D, N*(niter_MCMC - burnin) )).T).T)
        H_sampled_mapped_decoder = H_sampled_mapped_decoder.reshape(
                (F, N, niter_MCMC - burnin))

        speech_var_multi_samples = (H_sampled_mapped_decoder*
                                    self.g[:,:,None]) # shape (F,N,R)

        self.S_hat = np.mean(
                (speech_var_multi_samples/(speech_var_multi_samples
                                           + Phi_sampled*self.sigma_b2[:,:,np.newaxis])),
                                           axis=-1) * self.X

        self.N_hat = np.mean(
                (Phi_sampled*self.sigma_b2[:,:,np.newaxis]/(speech_var_multi_samples
                 + Phi_sampled*self.sigma_b2[:,:,np.newaxis])) , axis=-1) * self.X
