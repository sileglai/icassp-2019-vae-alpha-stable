#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright (c) 2019 by Inria
Authored by Simon Leglaive (simon.leglaive@inria.fr)
License agreement in LICENSE.txt
"""

import numpy as np
import librosa
import os
import pickle
from VAE import VAE, Decoder, Encoder
from MCEM_algo import MCEM_algo
from scipy.stats import levy_stable as alpha_stable

#%% Path to directories

# Directory to load the mixture
data_dir = 'audio'

# Directory to load the VAE model
vae_dir = 'training_results/latent_dim=64-2018-03-27-11h45'

#%% Parameters

fs = int(16e3) # Sampling rate
eps = np.finfo(float).eps # machine epsilon

# STFT parameters
wlen_sec = 64e-3 # STFT window length in seconds
hop_percent = 0.25  # hop size as a percentage of the window length
zp_percent=0
wlen = wlen_sec*fs # window length of 64 ms
wlen = np.int(np.power(2, np.ceil(np.log2(wlen)))) # next power of 2
hop = np.int(hop_percent*wlen) # hop size
nfft = wlen + zp_percent*wlen # number of points of the discrete Fourier transform
win = np.sin(np.arange(.5,wlen-.5+1)/wlen*np.pi); # sine analysis window

# Algorithm parameters
niter_MCEM = 200 # Maximum number of MCEM iterations
niter_MCMC = 40 # Number of iterations for the MCMC algorithm at the E-step
burnin = 30 # Burn-in period for the MCMC algorithm at the E-step
epsilon2 = 0.01 # Variance of the proposal distribution for the VAE latent variables
alpha = 1.8 # alpha-stable shape parameter

#%% Load mixture and compute STFT

mix_file = os.path.join(data_dir, 'mix.wav')
x, fs_x = librosa.load(mix_file, sr=None) # Load wav file without resampling
x = x/np.max(np.abs(x))
if fs != fs_x:
    raise ValueError('Unexpected sampling rate for the mixture signal')
T_orig = len(x)
x_pad = librosa.util.fix_length(x, T_orig + wlen // 2)
X = librosa.stft(x, n_fft=nfft, hop_length=hop, win_length=wlen, window=win)
F, N = X.shape

#%% Load VAE

# Load parameters
parms_pckl_file = os.path.join(vae_dir, 'parameters.pckl')
dic_params = pickle.load( open( parms_pckl_file, "rb" ))
for key, value in dic_params.items():
    if isinstance(value, str):
        exec("%s = '%s'" % (key, value))
    else:
        exec("%s = %s" % (key, value))


verbose_data = True
verbose_vae = 1
display_network = False

# Build VAE
vae = VAE(input_dim=input_dim, latent_dim=latent_dim,
          intermediate_dim=intermediate_dim, batch_size=1,
          activation=activation)      #%% Compute scores

vae.build()

# Load saved weights
weights_file = os.path.join(vae_dir, 'saved_weights.h5')
vae.load_weights(weights_file)

# Build decoder
decoder = Decoder(batch_size=N)
decoder.build(vae)

# Build encoder
encoder = Encoder(batch_size=N)
encoder.build(vae)

del vae

#%% Main block

# Compute the first VAE latent variable sample
latent_mean, latent_log_var = encoder.encode(np.abs(X).T**2)
H_init = latent_mean.T

# Compute the first impulse latent variable sample
scale_PaS = 2*np.cos(np.pi*alpha/4)**(2/alpha)
Phi_init = alpha_stable.rvs(alpha/2, 1, loc=0, scale=scale_PaS,
                            size=(F,N),
                            random_state=0)

# Instanciate the MCEM algo
mcem_algo = MCEM_algo(X=X, H=H_init, Phi=Phi_init, decoder=decoder, alpha=alpha,
                      niter_MCEM=niter_MCEM, niter_MCMC=niter_MCMC, burnin=burnin,
                      epsilon2=epsilon2)


# Run the MCEM algo
cost = mcem_algo.run()

# Separate the sources from the estimated parameters
mcem_algo.separate( niter_MCMC=niter_MCMC, burnin=burnin)

s_hat = librosa.istft(stft_matrix=mcem_algo.S_hat, hop_length=hop,
                      win_length=wlen, window=win, length=T_orig)
n_hat = librosa.istft(stft_matrix=mcem_algo.N_hat, hop_length=hop,
                      win_length=wlen, window=win, length=T_orig)

# Save estimated wav files
librosa.output.write_wav(os.path.join(data_dir, 'speech_est.wav'), s_hat, fs)
librosa.output.write_wav(os.path.join(data_dir,'noise_est.wav'), n_hat, fs)
